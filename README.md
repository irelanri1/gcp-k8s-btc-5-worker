# gke-k8s-btc-5-worker

Deploys our `bitcoind` worker onto our Kubernetes cluster in GCP/GKE, which was established across projects 2-4.

The K8S Terraform provider needs information about the (pre-existing) cluster, which is dynamic but this is currently hardcoded in the project - an obvious enhancement opportunity.

A Persistent Volume Claim is made (which uses GCP's default storage class, so we don't worry about defining the volume or even the class). This will be mounted by the Pods and used to write and persist the blockchain.

As a security measure it blocks ingress from pods in other namespaces.

A deployment is created.

A service is also created which load-balances the simple web server which each node will run in order to report blockchain info, so that we can easily monitor the progress and basic health of the bitcoin node cluster.
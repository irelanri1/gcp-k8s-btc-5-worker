resource "kubernetes_persistent_volume_claim" "btc-volume-claim" {
  
  metadata {
    name = "btc-volume-claim"
    namespace = "btc"
  }
  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        # Needs to be bigger than the BTC_PRUNE value to allow for memory flushing to disk
        storage = "4Gi"
      }
    }
    storage_class_name = "standard"
  }
  #timeouts {
    #create = "30m" #default is 10m
    #update = "10m" #default is 10m
    #delete = "10m" #default is 10m
  #}
}

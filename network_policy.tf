# Block traffic to specific pods from pods in other namespaces

resource "kubernetes_network_policy" "example" {
  metadata {
    name      = "btc-network-policy"
    namespace = "btc"
  }

  spec {
    pod_selector {
      match_expressions {
        key      = "name"
        operator = "In"
        values   = ["bitcoind"]
      }
    }

    # ingress {} with no ingress rule traffic from other namespaces is blocked
    egress {} # single empty rule to allow all egress traffic

    policy_types = ["Ingress", "Egress"]
  }
}
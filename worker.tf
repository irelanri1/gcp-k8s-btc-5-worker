

# Create a deployment (multiple pods)
resource "kubernetes_deployment" "bitcoind" {
  metadata {
    namespace = "btc"
    name = "bitcoind"
  }
  spec {
    replicas = 1 # how many pods to maintain
    selector { # pod selection
      match_labels = {
        app = "myapp"
      }
    }
    
    strategy {
      rolling_update {
        max_surge = 0 # we never want to go above '1' pod
        max_unavailable = 1 # we can take down the only pod, temporarily, during a rolling update
      }
    }

    template { # pods inside here
      metadata {
        namespace = "btc"
        labels = {
          name = "bitcoind"
          app = "myapp"
        }
      }
      spec {
        container {
          image = "irelanri/bitcoind:0.4"
          name  = "bitcoind"
          env {
            name = "BTC_PRUNE"
            value = 1050
          }
          volume_mount {
            name = "btc-volume"
            #mount_path = "/mydata"
            mount_path = "/bitcoin/data" # Mount point for all node data
          }
          
          # We want a graceful shutdown of the BTC node which flushes all cached data to disk
          lifecycle {
            pre_stop {
              exec {
                command = [ "/bitcoin/bin/pre-stop.sh" ]
              }
            }
          }
        }

        # Ensure our processes can write files to our mounted volume
        security_context {
            # group permissions assigned to mount points AND supplementary group for our in-container processes
            fs_group = 1000
            # our processes in container will not run as root 
            run_as_user = 1000
        }
        
        service_account_name = "btc-service-account"
        
        volume {
          name = "btc-volume"
          
          #host_path {
          #  path = "/Users/richard/btc-data"
          #  type = "DirectoryOrCreate"
          #}
          
          persistent_volume_claim {
            claim_name = "btc-volume-claim"
          }
          
        }
      }
    }
  }
  #timeouts {
  #  create = "30m" #default is 10m
  #  update = "10m" #default is 10m
  #  delete = "10m" #default is 10m
  #}
}


resource "kubernetes_service" "bitcoind" {
  metadata {
    name = "bitcoind"
    namespace = "btc"
    labels = {
        app = "bitcoind"
    }
  }
  spec {
    selector = {
      # select pods to place behind this service
      app = "myapp"
    }
    port {
      port = 80
      target_port = 8080
    }
    type = "LoadBalancer"
  }
}


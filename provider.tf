terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.72.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.3.2"
    }
  }
}

# Retrieve GKE cluster information
provider "google" {
  #project = data.terraform_remote_state.gke.outputs.project_id
  #region  = data.terraform_remote_state.gke.outputs.region
  region = "europe-west2"
  project = "leafy-mountain-317110"
}

# Configure kubernetes provider with Oauth2 access token.
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/client_config
# This fetches a new token, which will expire in 1 hour.
data "google_client_config" "default" {}

data "google_container_cluster" "my_cluster" {
  #name     = data.terraform_remote_state.gke.outputs.kubernetes_cluster_name
  #location = "data.terraform_remote_state.gke.outputs.region"
  name = "leafy-mountain-317110-gke"
  location = "europe-west2"
}

provider "kubernetes" {
  #host = data.terraform_remote_state.gke.outputs.kubernetes_cluster_host
  host = "35.197.230.127" # this will change IF I rebuild the cluster !!

  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(data.google_container_cluster.my_cluster.master_auth[0].cluster_ca_certificate)
}
